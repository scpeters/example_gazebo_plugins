#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/plugins/ImuSensorPlugin.hh>

#include <iostream>

namespace gazebo
{
  class MinimalImuPlugin : public ImuSensorPlugin
  {
    public: MinimalImuPlugin()
            : ImuSensorPlugin(),
            counter(0)
    {}

    public: virtual void Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf)
    {
      gzmsg << "Loading MinimalImuPlugin\n";
      ImuSensorPlugin::Load(_parent, _sdf);
      gzmsg << "Loaded MinimalImuPlugin\n";
    }

    public: virtual void OnUpdate(sensors::ImuSensorPtr _sensor) 
    {
      gzmsg << "IMU report #" << this->counter++ << "\n";
    }

    public: int counter;
  };
  
  GZ_REGISTER_SENSOR_PLUGIN(MinimalImuPlugin)
}
