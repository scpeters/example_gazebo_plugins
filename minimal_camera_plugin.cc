#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/plugins/CameraPlugin.hh>

#include <iostream>

namespace gazebo
{
  class MinimalCameraPlugin : public CameraPlugin
  {
    public: MinimalCameraPlugin()
            : CameraPlugin(),
            counter(0)
    {}

    public: virtual void Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf)
    {
      gzmsg << "Loading MinimalCameraPlugin\n";
      CameraPlugin::Load(_parent, _sdf);
      gzmsg << "Loaded MinimalCameraPlugin\n";
    }

    public: virtual void OnNewFrame(const unsigned char*, unsigned int, unsigned int, unsigned int, const std::string&)
    {
      gzmsg << "Camera report #" << this->counter++ << "\n";
    }

    public: int counter;
  };

  GZ_REGISTER_SENSOR_PLUGIN(MinimalCameraPlugin)
}
